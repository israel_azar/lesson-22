﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_22
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp first = new Camp(10, 20, 26, 13, 80);
            Camp second = new Camp(30, 60, 82, 41, 120);

            if (first > second)
            {
                Console.WriteLine("First Is Bigger");
            }
            else
            {
                Console.WriteLine("First Is Not Bigger");
            }

            Camp third = first + second;
            Console.WriteLine(third);
        }
    }
}
