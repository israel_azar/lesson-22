﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_22
{
    class Camp
    {
        private readonly int _id;
        public int Latitude { get; private set; }
        public int Longitude { get; private set; }
        public int NumberOfPeople { get; private set; }
        public int NumberOfTents { get; private set; }
        public int NumberOfFlashLights { get; private set; }
        private static int _last_camp_id = 0;

        public Camp(int latitude, int longitude, int numberOfPeople, int numberOfTents, int numberOfFlashLights)
        {
            Latitude = latitude;
            Longitude = longitude;
            NumberOfPeople = numberOfPeople;
            NumberOfTents = numberOfTents;
            NumberOfFlashLights = numberOfFlashLights;
            _last_camp_id++;
            _id = _last_camp_id;
        }
        public static bool operator == (Camp first,Camp second)
        {
            if (first is null && second is null)
            {
                return true;
            }
            if (first is null || second is null)
            {
                return false;
            }
            if (first._id == second._id)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator != (Camp first, Camp second)
        {
            return !(first == second);
        }
        public static bool operator >(Camp first, Camp second)
        {
            if (first is null || second is null)
            {
                return false;
            }
            return first.NumberOfPeople > second.NumberOfPeople;
        }
        public static bool operator < (Camp first, Camp second)
        {
            if (first is null || second is null)
            {
                return false;
            }
            return first.NumberOfPeople < second.NumberOfPeople;
        }
        public static Camp operator + (Camp first, Camp second)
        {
            Camp third = new Camp(first.Latitude + second.Latitude, first.Longitude + second.Longitude, first.NumberOfPeople + second.NumberOfPeople, first.NumberOfTents + second.NumberOfTents, first.NumberOfFlashLights + second.NumberOfFlashLights);
            return third;
        }

        public override bool Equals(object obj)
        {
            return this == obj as Camp;
        }

        public override int GetHashCode()
        {
            return this._id;
        }

        public override string ToString()
        {
            return "Camp Id: " + GetHashCode() + " Camp Latitude: " + Latitude + " Camp Longitude: " + Longitude + " Number Of People: " + NumberOfPeople + " Number Of Tents: " + NumberOfTents + " Number Of Flash Light: " + NumberOfFlashLights;
        }
    }
}
